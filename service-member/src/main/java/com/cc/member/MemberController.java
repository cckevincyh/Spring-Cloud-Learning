package com.cc.member;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MemberController {

    @Value("${server.port}")
    private String port;
    private static int count = 0;

    @RequestMapping("/getUserList")
    public List<String> getUserList() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count++;
        System.out.println(count);
        List<String> listUser = new ArrayList<String>();
        listUser.add("zhangsan");
        listUser.add("lisi");
        listUser.add("yushengjun");
        listUser.add(port);
        return listUser;
    }

    @RequestMapping("/getMemberInfo")
    public String getMemberInfo(){
        return "member server";
    }
}
