package com.cc.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private MemberService memberService;

    @RequestMapping("/getOrderByUserList")
    public List<String> getOrderByUserList(){
        return memberService.getOrderByUserList();
    }

    @RequestMapping("/getOrderInfo")
    public String getOrderInfo(){
        return "order server";
    }
}
