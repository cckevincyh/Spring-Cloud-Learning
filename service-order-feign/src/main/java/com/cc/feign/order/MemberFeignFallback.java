package com.cc.feign.order;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MemberFeignFallback implements MemberFeign {

    //服务降级处理
    public List<String> getOrderByUserList() {
        List<String> listUser = new ArrayList<String>();
        listUser.add("not user list");
        return listUser;
    }
}

