package com.cc.feign.order;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(value="service-member",fallback= MemberFeignFallback.class)
public interface MemberFeign {
	@RequestMapping("/getUserList")
	public List<String> getOrderByUserList();
}
