package com.cc.feign.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MemberFeignController {

    @Autowired
    private MemberFeign memberFeign;

    @RequestMapping("/getUserList")
    public List<String> getUserList(){
        return memberFeign.getOrderByUserList();
    }

    @RequestMapping("/getOrderInfo")
    public String getOrderInfo(){
        return "order server";
    }
}


